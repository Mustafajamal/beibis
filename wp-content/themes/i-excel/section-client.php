<?php
/**
 * Template Name: Clients
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
 
// Work WP_Query arguments
?>
<?php
get_header();
?>
<section id="clients" class="default-clients">
        <h2 id="clients-h2">&nbsp;PrintedWalls Clients&nbsp;</h2>
		<?php
		while ( have_posts() ) : the_post();
			the_content();
			// End of the loop.
		endwhile;
		?>
		<div class="client_container">
            <div class="clearfix"></div>
			<div class="row">
				<div class="col-sm-2">
					<img src="http://printedwalls.co.uk/wp-content/uploads/2016/09/black-decer-logo.jpg" class="attachment-thumbnail" alt="tesco-logo">
					
				</div>
				<div class="col-sm-2">
					<img src="http://printedwalls.co.uk/wp-content/uploads/2016/09/papajhons-logo-150x79.jpg" class="attachment-thumbnail" alt="papajhons-logo">
				</div>
				<div class="col-sm-2">
					<img src="http://printedwalls.co.uk/wp-content/uploads/2016/09/tesco-logo.jpg" class="attachment-thumbnail" alt="tesco-logo">
				</div>
				<div class="col-sm-2">
					<img src="http://printedwalls.co.uk/wp-content/uploads/2016/09/lincoln-logo.jpg" class="attachment-thumbnail" alt="lincoln-logo">
				</div>
				<div class="col-sm-2">
					<img src="http://printedwalls.co.uk/wp-content/uploads/2016/10/tarmac-logo.jpg" class="attachment-thumbnail" alt="tarmac-logo">
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-2">
					<img src="http://printedwalls.co.uk/wp-content/uploads/2016/09/knockando-logo.jpg" class="attachment-thumbnail" alt="knockando-logo">
				</div>
				<div class="col-sm-2">
					<img width="143" height="68" src="http://printedwalls.co.uk/wp-content/uploads/2016/09/runbritain-logo.jpg" class="attachment-thumbnail" alt="runbritain-logo">				</div>
				<div class="col-sm-2">
					<img width="143" height="84" src="http://printedwalls.co.uk/wp-content/uploads/2016/09/imb-logo.jpg" class="attachment-thumbnail" alt="imb-logo">				</div>
				<div class="col-sm-2">
					<img width="135" height="81" src="http://printedwalls.co.uk/wp-content/uploads/2016/09/redbull-logo.jpg" class="attachment-thumbnail" alt="redbull-logo">				</div>
				<div class="col-sm-2">
					<img width="150" height="61" src="http://printedwalls.co.uk/wp-content/uploads/2016/09/gerbing-logo-150x61.jpg" class="attachment-thumbnail" alt="gerbing-logo">				</div>
			</div>
			<div class="row">
				<div class="col-sm-2">
					<img width="121" height="100" src="http://printedwalls.co.uk/wp-content/uploads/2016/09/printed-wall-our-clients.jpg" class="attachment-thumbnail" alt="printed-wall-our-clients">				</div>
				<div class="col-sm-2" id="bognor">
					<img width="130" height="108" src="http://printedwalls.co.uk/wp-content/uploads/2016/09/bognor-registownfc-logo.jpg" class="attachment-thumbnail" alt="bognor-registownfc-logo">				</div>
				<div class="col-sm-2">
					<img src="http://printedwalls.co.uk/wp-content/uploads/2016/09/we-are-macmillan-logo.jpg" class="attachment-thumbnail" alt="we-are-macmillan-logo">
				</div>
				<div class="col-sm-2">
					<img src="http://printedwalls.co.uk/wp-content/uploads/2016/09/stanely-logo.jpg" class="attachment-thumbnail" alt="lincoln-logo">
				</div>
				<div class="col-sm-2">
					<img src="http://printedwalls.co.uk/wp-content/uploads/2016/09/dewalt-logo.jpg" class="attachment-thumbnail" alt="dewalt-logo">
				</div>
			</div>
			
            <!--
            Do not remove these lines to make portfolio visible -->
		</div>
	</section><?php

get_template_part('footer-top');
get_footer(); ?>