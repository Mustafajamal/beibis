<?php
/** Template Name: Login
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<?php 
get_header();
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
		// Post thumbnail.
	
	$login_state=$_GET["login"];
	
	?>
	<div class="login_block entry-content">
		<?php if($login_state=="failed"){
		echo "<p style='color: red;'>The username or password entered are incorrect, please try again.</p>";	
		}
		?>
		<div class="login_head">
		<h2 id="h1_login">&nbsp;LOGIN&nbsp;</h2>
		</div><!-- .entry-header -->
		<div class="login_section row">
			<div class="tb_login_form col-sm-7">
				<?php 
				wp_login_form();
				?>
				<div class="forgot_link">
				<a href="<?php echo wp_lostpassword_url(); ?>" title="Lost Password">Forgotten Password?</a>
				</div>
			</div>
			<div class="tb_login_user col-sm-5 hidden-xs">
			<img src="http://printedwalls.co.uk/wp-content/uploads/2016/10/login-img.png">
			</div>
		</div>
	</div>
</article><!-- #post-## -->
<?php
get_template_part('footer-top');
get_footer(); ?>