<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package i-excel
 * @since i-excel 1.0
 */
?>
<?php


$bg_style = '';
$top_phone = '';
$top_email = '';
$topbar_bg_class = '';


$topbar_bg = get_theme_mod('topbar_bg', 1);
$top_phone = esc_attr(get_theme_mod('top_phone', '1-000-123-4567'));
$top_email = esc_attr(get_theme_mod('top_email', 'email@i-create.com'));
$iexcel_logo = get_theme_mod( 'logo', get_template_directory_uri().'/images/logo.png' );

if ( $topbar_bg == 1 )
{
	$topbar_bg_class = "colored-bg";
}

global $post; 

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<?php    
    if ( ! function_exists( '_wp_render_title_tag' ) ) :
        function iexcel_render_title() {
    ?>
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <?php
        }
        add_action( 'wp_head', 'iexcel_render_title' );
    endif;    
    ?> 
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?> style=" <?php echo $bg_style; ?> ">
	<div id="page" class="hfeed site">
    	
        <?php if ( $top_email || $top_phone || iexcel_social_icons() ) : ?>
    	<div id="utilitybar" class="utilitybar <?php echo $topbar_bg_class; ?>">
        	<div class="ubarinnerwrap">
                <div class="socialicons">
                    <?php echo iexcel_social_icons(); ?>
                </div>
                <?php if ( !empty($top_phone) ) : ?>
                <div class="topphone">
                    <i class="topbarico genericon genericon-phone"></i>
                    <?php _e('Call us : ','i-excel'); ?> <?php echo esc_attr($top_phone); ?>
                </div>
                <?php endif; ?>
                
                <?php if ( !empty($top_email) ) : ?>
                <div class="topphone">
                    <i class="topbarico genericon genericon-mail"></i>
                    <?php _e('Mail us : ','i-excel'); ?> <?php echo sanitize_email($top_email); ?>
                </div>
                <?php endif; ?>                
            </div> 
        </div>
        <?php endif; ?>
        
        <div class="header_container">
            <header id="masthead" class="site-header" role="banner">
         		<div class="headerinnerwrap">
					<div class="header_top">
					
					<?php if ($iexcel_logo) : ?>
						<a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
							<span><img src="<?php echo esc_url($iexcel_logo); ?>" alt="<?php bloginfo( 'name' ); ?>" /></span>
						</a>
					<?php else : ?>
						<span id="site-titlendesc">
							<a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
								<h1 class="site-title"><?php bloginfo( 'name' ); ?></h1>
								<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>   
							</a>
						</span>
					<?php endif; ?>	
						<div class="row header_row">
							<div class="col-sm-2 pw_header_icon pw_live">
							<img src="http://efound.co.uk/wallpapers/wp-content/uploads/2016/08/support-icon.png"><div>Live Chat Support</div>
							</div>
							<div class="col-sm-2 pw_header_icon">
							<img src="http://efound.co.uk/wallpapers/wp-content/uploads/2016/08/guarntee-icon.png"><div>Satisfaction Guarantee</div>
							</div>
							<div class="col-sm-3 pw_serch">
							<?php get_search_form(); ?>
							</div>
							<div class="col-sm-2 pw_login">
							<a href="<?php echo wp_login_url(); ?>" title="Login">Login</a> / 
							<a href="<?php echo site_url().'/register'; ?>">Register</a>
							<p>0161 408 5474</p>
							</div>
							<a class="cart-icon" href="<?php echo wc_get_cart_url();?>" >
							<div class="col-sm-1 pw_cart">
								<?php if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
									?> <span class="wc_count">
									 <?php
									 echo WC()->cart->get_cart_contents_count(); ?>
									 
								<?php } ?>							
							</div>
							</a>
						</div>
					</div>
                    <div id="navbar" class="navbar">
                        <nav id="site-navigation" class="navigation main-navigation" role="navigation">
                            <a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'i-excel' ); ?>"><?php _e( 'Skip to content', 'i-excel' ); ?></a>
                            <?php 
								if ( has_nav_menu(  'primary' ) ) {
										wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu', 'container_class' => 'nav-container', 'container' => 'div' ) );
									}
									else
									{
										wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-container' ) ); 
									}
								?>
							
                        </nav><!-- #site-navigation -->
                    </div><!-- #navbar -->
                    <div class="clear"></div>
                </div>
            </header><!-- #masthead -->
        </div>

        <!-- #Banner -->
		<span class="img_link">
		<a href="<?php echo site_url().'/printedwalls-categories/';?>"><img src="<?php echo site_url().'/wp-content/uploads/2016/08/select-wallpaper.png';?>"></a>
		<a href="<?php echo site_url().'/upload-image/';?>"><img src="<?php echo site_url().'/wp-content/uploads/2016/08/upload-artwork.png';?>"></a>
		</span>
		<?php putRevSlider( "header","homepage" ) ?>
        <!-- #End Banner -->
       
		<div id="main" class="site-main">
		<div class="social_links">
		<a href="#"><img src="<?php echo site_url().'/wp-content/uploads/2016/10/secoial-media-icons_01.gif';?>"></a>
		<a href="#"><img src="<?php echo site_url().'/wp-content/uploads/2016/10/secoial-media-icons_02.png';?>"></a>
		<a href="#"><img src="<?php echo site_url().'/wp-content/uploads/2016/10/secoial-media-icons_03_01.png';?>"></a>
		</div>
		