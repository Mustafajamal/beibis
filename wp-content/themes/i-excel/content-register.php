<?php
/** Template Name: Register
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<?php 
get_header();
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<?php
	if(isset($_POST['submit_user'])){
				
				
				$rt_firstname = $_POST['rt_firstname'];
				$rt_lastname = $_POST['rt_lastname'];
				$rt_email = $_POST['rt_email'];
				$rt_password = $_POST['rt_password'];
				$rt_address = $_POST['rt_address'];
				$rt_phone1 = $_POST['rt_phone1'];
				$rt_phone2 = $_POST['rt_phone2'];
				$rt_company_name = $_POST['rt_company_name'];
				$rt_company_desc = $_POST['rt_company_desc'];
				$rt_company_address = $_POST['rt_company_address'];
				$rt_company_zipcode = $_POST['rt_company_zipcode'];
				$rt_company_website = $_POST['rt_company_website'];
				
				
				
					if ( !username_exists($rt_firstname) && !email_exists($rt_email) )
					{
						// Create user and set role to administrator
						$userdata = array(
							'user_login'  =>  $rt_firstname,
							'first_name'  =>  $rt_firstname,
							'last_name'  =>  $rt_lastname,
							'user_url'    =>  $rt_company_website,
							'user_pass'   =>  $rt_password,
							'user_email'   =>  $rt_email,
							'billing_company'   =>  $rt_company_name,
							'billing_address_1'   =>  $rt_company_address,
							'billing_postcode'   =>  $rt_company_zipcode,
							'billing_phone'   =>  $rt_phone1
							
						);
						
						
						$user_id = wp_insert_user( $userdata);
				
						if ( is_int($user_id) )
						{
																					
							$wp_user_object = new WP_User($user_id);
							$wp_user_object->set_role('customer');
							
							update_user_meta( $user_id, 'billing_first_name', sanitize_text_field( $_POST['rt_firstname'] ) );
							update_user_meta( $user_id, 'billing_last_name', sanitize_text_field( $_POST['rt_lastname'] ) );
							update_user_meta( $user_id, 'billing_company', sanitize_text_field( $_POST['rt_company_name'] ) );
							update_user_meta( $user_id, 'billing_address_1', sanitize_text_field( $_POST['rt_company_address'] ) );
							update_user_meta( $user_id, 'billing_phone', sanitize_text_field( $_POST['rt_phone1'] ) );
							update_user_meta( $user_id, 'shipping_postcode', sanitize_text_field( $_POST['rt_company_zipcode'] ) );

						//	echo 'Successfully created new admin user. Now delete this file!';
						echo"<div class='alert alert-success'>User has been registered successfully.</div>";
						//echo"<div class='alert alert-info'>We would approve your account after short review and let you know via email.</div>";
						
						}
						else {
						echo"<div class='alert alert-danger'>Fatel Error. Soory No user was Created!</div>";
						}
					}
					else {
						echo"<div class='alert alert-info'>User with this Username/Email already exists.!</div>";
						
					}
						
				///User Register End
				
		
			}
			?>
		<div class="register_form">
			<form action="" id="sup" method="post" enctype="multipart/form-data"  method="post">
				<div class="login_head">
					<h2 id="h1_login">&nbsp;Registration&nbsp;</h2>
				</div><!-- .entry-header -->
				<div id="register" class="login_section row">
					<div class="col-sm-6">	
						<div class="form-group">
							<label for="name"><span class="steric-red">*</span>First Name</label>
							<input id="rt_firstname" placeholder="John" type="text" name="rt_firstname" class="inputfield form-control" required>
						</div>
					</div>
						
				
					<div class="col-sm-6">
						<div class="form-group">
							<label for="name">Last Name</label>
							<input type="text"  placeholder="Alley" name="rt_lastname" id="rt_lastname" class="inputfield form-control" required>
						</div>				
					</div>				
					
					
					<div class="col-sm-6">
						<div class="form-group">
							<label for="email"><span class="steric-red">*</span>Email</label>
							<input type="email"  placeholder="example@xyz.com" name="rt_email" class="inputfield form-control" id="rt_email" required>
						</div>
					</div>
						
					<div class="col-sm-6">	
						<div class="form-group">
							<label for="pwd"><span class="steric-red">*</span>Password:</label>
							<input type="password"  placeholder="*****" name="rt_password" class="inputfield form-control" id="rt_password" required>
						</div>
					</div>
						
					<div class="col-sm-6">	
						<div class="form-group">
							<label for="phone"><span class="steric-red">*</span>Phone Number</label>
							<input type="text"  placeholder="+44 20 7123 4567" name="rt_phone1" class="inputfield form-control" id="rt_phone1" required>
						</div>
					</div>
					
					<div class="col-sm-6">					
						<div class="form-group">
							<label for="phone">Mobile</label>
							<input type="text" placeholder="+44 20 7123 4567" name="rt_phone2" class="inputfield form-control" id="rt_phone2" >
						</div>
					</div>
				</div>
					<h2 id="h2_company">&nbsp;Company Details&nbsp;</h2>
				<div id="company" class="login_section row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="address"><span class="steric-red">*</span>Company Name</label>
							<input type="text" placeholder="Example inc" name="rt_company_name" class="inputfield form-control" id="address" required>
						</div>
					</div>
					<div class="col-sm-6">	
						<div class="form-group">
							<label for="country">Company Website</label>
							<input type="text"  placeholder="www.example.org"  name="rt_company_website" class="inputfield form-control" id="country" >
						</div>																		
					</div>
					<div class="col-sm-6">	
						<div class="form-group">
							<label for="state">Company Address</label>
							<input type="text" placeholder="Address of your company" name="rt_company_address" class="inputfield form-control" id="state" >
						</div>
					</div>
						
					<div class="col-sm-6">	
						<div class="form-group">
							<label for="postalcode">Company Postal code</label>
							<input type="text" placeholder="Postal/Zip code" name="rt_company_zipcode" class="inputfield form-control" id="zipcode" >
						</div>
					</div>
					
					<div class="col-sm-12 submit-col">														
					<!--<input  type="submit" name="submit_cook" value="SEND" class="btn-send btn-block btn"> -->
					<button type="submit" name="submit_user" class="register-btn">Submit&nbsp;<i class="fa fa-angle-double-right" style="font-size:32px"></i></button>
					</div>					
				</div>
					<div class="left_line"></div>
					<div class="right_line"></div>				
			</form>
		</div>
</article><!-- #post-## -->
<?php
get_template_part('footer-top');
get_footer(); ?>