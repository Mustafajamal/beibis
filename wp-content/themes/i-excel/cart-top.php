<?php
/**
 * The template for displaying the cart top
 *
 * Contains cart top content and the closing of the #main and #page div elements.
 *
 * @package i-excel
 * @since i-excel 1.0
 */
?>
<div class="container">
	<div class="row cart-top">
		<div id="cart-basket" class="col-sm-4">
			<img src="http://printedwalls.co.uk/wp-content/uploads/2016/09/cart-basket-icon.png">
			<div class="bar1"></div>
		</div>
		<div id="cart-invoice" class="col-sm-4">
			<img src="http://printedwalls.co.uk/wp-content/uploads/2016/09/cart-invoice-icon.png">
			<div class="bar2"></div>
		</div>
		
		<div id="complete-order" class="col-sm-4">
			<img src="http://printedwalls.co.uk/wp-content/uploads/2016/09/complete-order-icon.png">
		</div>
	</div>
	<div class="row cart-top-text">
		<div class="col-sm-4">
			<h5>Your Basket</h5>
			<img id="cart-arrow-basket" class="cart-arrow" src="http://printedwalls.co.uk/wp-content/uploads/2016/09/selected-arrow-down.png">
		</div>
		<div class="col-sm-4">
			<h5>Make Your Payment</h5>
			<img id="cart-arrow-payment" class="cart-arrow" src="http://printedwalls.co.uk/wp-content/uploads/2016/09/selected-arrow-down.png">			
		</div>
		<div class="col-sm-4">
			<h5>Order Completed</h5>
			<img id="cart-arrow-order" class="cart-arrow" src="http://printedwalls.co.uk/wp-content/uploads/2016/09/selected-arrow-down.png">			
		</div>
	</div>
</div>