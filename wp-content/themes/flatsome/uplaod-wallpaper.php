<?php
/**
 * Template Name: Upload Wallpaper
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
get_header();
?>
    <div class="row">
        <div class="upload_wall_section col small-12 vcenter">
			<?php echo do_shortcode('[custom_wallpaper]'); ?>
        </div>
        <!--<div class="upload_wall_size col medium-4 large-4">
				<h3>Enter your wall size</h3>
            <div style="background: rgb(230, 230, 230) none repeat scroll 0% 0%; padding: 16px;">
			
                <div class="row" style="display: none;">
                    <div class="small-3 columns">
                        <label for="middle-label" class="middle">Canvas Size: </label>
                    </div>
                    <div class="small-9 columns">
                        <select id="unit" onchange="javascript:unitConversion(this.value);">                            
                            <option selected="" value="foot">Foot</option>
                            <option value="cm" selected>CM</option>
                            <option value="m">Meter</option>
                            <option value="inches">Inches</option>
                            <option value="mm">MM</option>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="small-3 columns">
                        <label for="design_yout_wall_width" class="middle">Width: </label>
                    </div>
                    <div class="small-9 columns">
                        <input type="text" id="design_yout_wall_width" value="<?php //echo get_option('wdyw_default_width'); ?>"> 
                    </div>
                </div>
                <div class="row">
                    <div class="small-3 columns">
                        <label for="design_yout_wall_height" class="middle">Height: </label>
                    </div>
                    <div class="small-9 columns">
                        <input type="text" id="design_yout_wall_height" value="<?php //echo get_option('wdyw_default_height'); ?>"> 
                    </div>
                </div>
                <div class="row">
                    <div class="small-3 columns">
                        <label for="middle-label" class="middle">Canvas Finishing: </label>
                    </div>
                    <div class="small-9 columns">
                        <select name="finishing" id="finishing" onchange="javascript:updateFinishingPrice()">
                            <option value="Tiles">Synthetic</option>
                            <option value="One Piece">Natural</option>                            
                        </select>
                    </div>
                </div> 

                <input type="text" id="resetJcrop" onclick="javascript:updateFinishingPrice()" style="display:none;" />
            </div>
            <div style="width: 100%; text-align: center; font-weight: bold; padding: 12px; font-size: 32px;">Price : <span><?php //echo get_woocommerce_currency_symbol(); ?><span id="total_price"><?php //echo get_option('wdyw_min_price') * get_option('wdyw_default_width') * get_option('wdyw_default_height'); ?></span></span></div>
            <a id="wdyw_add_to_cart" onclick="wdyw_add_to_cart()"><img src="http://printedwalls.co.uk/wp-content/uploads/2016/09/add-to-cart-img.png" /></a>
        </div>-->
    </div>
	<!--<div class="tab_container">
      <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">Information</a></li>
        <li><a data-toggle="tab" href="#menu1">Installation</a></li>
        <li><a data-toggle="tab" href="#menu2">Delivery</a></li>
        <li><a data-toggle="tab" href="#menu3">Copyright</a></li>
        <li><a data-toggle="tab" href="#menu3">Print</a></li>
      </ul>

      <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
    	  <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
        </div>
        <div id="menu1" class="tab-pane fade">
          <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
        </div>
        <div id="menu2" class="tab-pane fade">
          <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
        </div>
        <div id="menu3" class="tab-pane fade">
          <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
        </div>
    	<div id="menu4" class="tab-pane fade">
          <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
        </div>
      </div>
    </div>-->
<?php get_template_part('footer-top'); ?>
<?php get_footer(); ?>