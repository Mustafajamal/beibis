<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
get_header();

while (have_posts()) : the_post();
    ?>
    <div class="container-fluid">
    <div class="row row-full-width main_container_designer">
    <script type="text/javascript">
      // jQuery(document).ready(function() {

      //     jQuery(document).ready(function(){  
      //       //document.body.innerText = 'checking';
      //       if (localStorage.getItem('isLoaded') !== 'yes') {
      //         localStorage.setItem('isLoaded', 'yes');
      //         console.log('reloading now');
      //            // document.body.innerText += ' - loaded once';
      //         location.reload();
      //       } else {
      //         console.log('not reloading');
      //             //document.body.innerText += ' - loaded twice';
      //       }
      //     });
      // });
    </script>  
        <div class="left_main col small-12 medium-12 large-3">
            <div class="select_size_main col medium-12 large-12">
                <h3>Canvas Format: </h3>
                <div class="selectcanvassize ">
                    <input checked="checked" id="vertical" type="radio" name="canvassize" onclick="canvasSize(this);" value="vertical" />
                    <label class="drinkcard-cc vertical" for="vertical"></label>
                    <input id="horizontal" type="radio" name="canvassize" onclick="canvasSize(this);" value="horizontal" />
                    <label class="drinkcard-cc horizontal" for="horizontal"></label>
                    <input id="square" type="radio" name="canvassize" onclick="canvasSize(this);" value="square" />
                    <label class="drinkcard-cc square" for="square"></label>
                </div>
                <div class="vertical_main">
                  <input id="20x30" onclick="getSelectedSize(this);" width="20" height="30" type="radio" name="cansizerad" value="20x30" checked><label for="20x30">20x30cm</label><br>
                  <input id="30x40" onclick="getSelectedSize(this);" width="30" height="40" type="radio" name="cansizerad" value="30x40"><label for="30x40">30x40cm</label><br>
                  <input id="40x50" onclick="getSelectedSize(this);" type="radio" name="cansizerad" value="40x50" ><label for="40x50">40x50cm</label><br>  
                  <input id="40x60" onclick="getSelectedSize(this);" type="radio" name="cansizerad" value="40x60"><label for="40x60">40x60cm</label><br>  
                  <input id="40x70" onclick="getSelectedSize(this);" type="radio" name="cansizerad" value="40x70"><label for="40x70">40x70cm</label><br>  
                  <input id="50x60" onclick="getSelectedSize(this);" type="radio" name="cansizerad" value="50x60"><label for="50x60">50x60cm</label><br>  
                  <input id="50x70" onclick="getSelectedSize(this);" type="radio" name="cansizerad" value="50x70"><label for="50x70">50x70cm</label><br>  
                  <input id="50x90" onclick="getSelectedSize(this);" type="radio" name="cansizerad" value="50x90"><label for="50x90">50x90cm</label><br>  
                  <input id="60x90" onclick="getSelectedSize(this);" type="radio" name="cansizerad" value="60x90"><label for="60x90">60x90cm</label><br>  
                  <input id="70x100" onclick="getSelectedSize(this);" type="radio" name="cansizerad" value="70x100"><label for="70x100">70x100cm</label><br>  
                  <input id="80x120" onclick="getSelectedSize(this);" type="radio" name="cansizerad" value="80x120"><label for="80x120">80x120cm</label><br>  
                  <input id="100x150" onclick="getSelectedSize(this);" type="radio" name="cansizerad" value="100x150"><label for="100x150">100x150cm</label><br>  
                </div>  
                <div class="horizontal_main col">
                  <input id="30x20" type="radio" onclick="getSelectedSize(this);" name="cansizerad" value="30x20"><label for="30x20">30x20cm</label><br>
                  <input id="40x30"id="30x20"type="radio" onclick="getSelectedSize(this);" name="cansizerad" value="40x30"><label for="40x30">40x30cm</label><br>
                  <input id="50x40"type="radio" onclick="getSelectedSize(this);" name="cansizerad" value="50x40"><label for="50x40">50x40cm</label><br>  
                  <input id="60x40" type="radio" onclick="getSelectedSize(this);" name="cansizerad" value="60x40"><label for="60x40">60x40cm</label><br>  
                  <input id="70x40" type="radio" onclick="getSelectedSize(this);" name="cansizerad" value="70x40"><label for="70x40">70x40cm</label><br>  
                  <input id="60x50" type="radio" onclick="getSelectedSize(this);" name="cansizerad" value="60x50"><label for="60x50">60x50cm</label><br>  
                  <input id="70x50" type="radio" onclick="getSelectedSize(this);" name="cansizerad" value="70x50"><label for="70x50">70x50cm</label><br>  
                  <input id="90x50" type="radio" onclick="getSelectedSize(this);" name="cansizerad" value="90x50"><label for="90x50">90x50cm</label><br>  
                  <input id="90x60" type="radio" onclick="getSelectedSize(this);" name="cansizerad" value="90x60"><label for="90x60">90x60cm</label><br>  
                  <input id="100x70" type="radio" onclick="getSelectedSize(this);" name="cansizerad" value="100x70"><label for="100x70">100x70cm</label><br>  
                  <input id="120x80" type="radio" onclick="getSelectedSize(this);" name="cansizerad" value="120x80"><label for="120x80">120x80cm</label><br>  
                  <input id="150x100" type="radio" onclick="getSelectedSize(this);" name="cansizerad" value="150x100"><label for="150x100">150x100cm</label><br>  
                </div>  
                <div class="square_main">
                  <input id="20x20" type="radio" onclick="getSelectedSize(this);" name="cansizerad" value="20x20"><label for="20x20">20x20cm</label><br>
                  <input id="30x30" type="radio" onclick="getSelectedSize(this);" name="cansizerad" value="30x30"><label for="30x30">30x30cm</label><br>
                  <input id="40x40" type="radio" onclick="getSelectedSize(this);" name="cansizerad" value="40x40"><label for="40x40">40x40cm</label><br>  
                  <input id="50x50" type="radio" onclick="getSelectedSize(this);" name="cansizerad" value="50x50"><label for="50x50">50x50cm</label><br>  
                  <input id="60x60" type="radio" onclick="getSelectedSize(this);" name="cansizerad" value="60x60"><label for="60x60">60x60cm</label><br>  
                  <input id="70x70" type="radio" onclick="getSelectedSize(this);" name="cansizerad" value="70x70"><label for="70x70">70x70cm</label><br>  
                  <input id="80x80" type="radio" onclick="getSelectedSize(this);" name="cansizerad" value="80x80"><label for="80x80">80x80cm</label><br>  
                  <input id="90x90" type="radio" onclick="getSelectedSize(this);" name="cansizerad" value="90x90"><label for="90x90">90x90cm</label><br>  
                  <input id="100x100" type="radio"onclick="getSelectedSize(this);"  name="cansizerad" value="100x100"><label for="100x100">100x100cm</label><br> 
                </div> 
                  
            </div>
            <div class="can_type_main col small-12 large-12 " >
                
                 <div class="can_type_inner">
                    <h3>Canvas Type</h3>
                    <!--<select name="canvas_type" id="canvas_type" >
                        <option value="natural">Natural</option>
                        <option value="synthetic">Synthetic</option>                            
                    </select>-->
                    <input id="natural" type="radio" name="canvas_type" value="natural" checked><label for="natural">Natural</label><br>
                    <input id="synthetic" type="radio" name="canvas_type" value="synthetic"><label for="synthetic">Synthetic</label><br>
                </div> 
                <div class="lac_inner">
                    <h3>lacquered: </h3>                
                    <!--<select name="material" id="material" onchange="set_material()">
                        <option value="non lacquered" selected>Non lacquered</option> 
                        <option value="lacquered"> lacquered</option>
                    </select> -->
                    <input id="non lacquered" type="radio" name="material" value="non lacquered" checked><label for="non lacquered">Non lacquered</label><br>
                    <input id="lacquered" type="radio" name="material" value="lacquered"><label for="lacquered">lacquered</label><br>
                </div>
                <div class="col small-4" style="display: none;">                    
                    <div class="">
                        <select name="finishing" id="finishing" onchange="set_finishing();">
                            <option value="tiles">Tiles</option>
                            <option value="one_piece">One Piece</option>                            
                        </select>
                    </div>
                </div>

                  
            </div>
        </div>




        <!--<div class="wall_section col small-12 medium-6 large-6" style="padding-right: 0px !important;padding-left: 7px;">-->
        <div class="wall_section col small-12 medium-12 large-6" >
		 
            <div class="img_container" style="position:relative;">
                <?php
                if (isset($_GET['filename'])) {
                    $attachment = (object) array();
                    $attachment->guid = urldecode($_GET['filename']);
                    $parsed_url = parse_url($attachment->guid);
                    $attachment_path = $_SERVER['DOCUMENT_ROOT'] . $parsed_url['path'];
                } else {
                    $attachment_path = get_attached_file(get_post_thumbnail_id());
                    $attachment = get_post(get_post_thumbnail_id());
                }
                list($width, $height) = getimagesize($attachment->guid);
               // print_r($width);
               // print_r($height);


               
                $round_hundred = ceil($width / 100) * 100;
                
                $round_hundred_w = ceil($width / 100) * 100;
                $round_hundred_h = ceil($height / 100) * 100;

                //Round for square
                $round_hundred_w_square = floor($width / 4) ;
                $round_hundred_h_square = floor($height / 4);
                
                ?>

                <script type="text/javascript">
                    
                        jQuery(function($) {
                        var d = document, ge = 'getElementById';

                        $('#interface').on('cropmove cropend', function(e, s, c) {
                            d[ge]('crop-x').value = c.x;
                            d[ge]('crop-y').value = c.y;
                            d[ge]('crop-w').value = c.w;
                            d[ge]('crop-h').value = c.h;
            							var canvas_top = c.y - 131;
            							var canvas_left_top = c.x - 123;
            							var canvas_left_bottom = c.y + 25;
            							var canvas_top_top = canvas_top + 30;

                           var current_top = jQuery(".jcrop-selection.jcrop-current").css("top");
                           var current_left = jQuery(".jcrop-selection.jcrop-current").css("left");
                           var current_bottom = jQuery(".jcrop-selection.jcrop-current").css("bottom");
                           
                           var current_top = parseInt(current_top) - parseInt(30);
                           var current_height_top = parseInt(current_top) + parseInt(30);
                           var current_left = parseInt(current_left) - parseInt(30);
                           var current_widthleft = parseInt(current_left) + parseInt(28);

                           jQuery("#lable_width").css({"top": current_top , "left": current_widthleft});
						               jQuery("#lable_height").css({"top": current_height_top , "bottom": current_bottom , "left": current_left});
                            
                           
                        });
                         

                       // $('#target').Jcrop({                            
                       //      //width: <?php //echo $width; ?>, 
                       //      //height: <?php //echo $height; ?>,

                       //      width: '750', 
                       //      height: '900',  
                       //      boxWidth: '750', 
                       //      boxHeight: '900',
                            
                       //      //setSelect: [0, 0, <?php //echo $round_hundred; ?>, <?php //echo $round_hundred; ?>],
                       //      //allowResize: false,
                       //      //allowSelect: false 
                       //      setSelect: [0, 0,0,500],
                       //      aspectRatio: 1 / 2 ,
                       //      //aspectRatio: <?php //echo get_option('wdyw_default_width'); ?> / <?php //echo get_option('wdyw_default_height'); ?> 
                          
                            
                       //  }); 

                         
                           
                        $('#text-inputs').on('change', 'input', function(e) {
                            $('#target').Jcrop('api').animateTo([
                                parseInt(d[ge]('crop-x').value),
                                parseInt(d[ge]('crop-y').value),
                                parseInt(d[ge]('crop-w').value),
                                parseInt(d[ge]('crop-h').value)
                            ]);
                        });

                        // $('#design_yout_wall_width, #design_yout_wall_height').focusout(function() {
                        //     x = document.getElementById('design_yout_wall_width').value;
                        //     y = document.getElementById('design_yout_wall_height').value;
                        //     $('#target').Jcrop({boxWidth: <?php //echo $width; ?>, boxHeight: <?php //echo $height; ?>, setSelect: [0, 0, <?php //echo $round_hundred_w; ?>, <?php //echo $round_hundred_h; ?>], aspectRatio: x / y, onSelect: updateCoords,allowResize: true});
                        // });
                        
                        jQuery("input[name='canvas_type']").click(function(){    
                          jQuery("#design_yout_wall_width").keyup();
                          jQuery("#design_yout_wall_height").keyup(); 
                        });
                        jQuery("input[name='material']").click(function(){    
                          jQuery("#design_yout_wall_width").keyup();
                          jQuery("#design_yout_wall_height").keyup(); 
                        });
                        $('#design_yout_wall_width, #design_yout_wall_height').keyup(function() {

                            // var invalidChars = /[^0-9.]/gi
                            // if (invalidChars.test($(this).val())) {
                            //     $(this).val() = $(this).val().replace(invalidChars, "");
                            // }
                            x = document.getElementById('design_yout_wall_width').value;
                            y = document.getElementById('design_yout_wall_height').value;
            
                            foot_width = document.getElementById('design_yout_wall_width').value;
                            foot_height = document.getElementById('design_yout_wall_height').value;
                                    
                            total_sq_cm = foot_width * foot_height;
                            total_sq_cm = total_sq_cm.toFixed(2);
                            total_price = 0.00;

                            var finishing = jQuery('#finishing').val();
                            if (finishing == "One Piece") {
                                add_finishing_total = one_piece_per * total_price / 100;
                                total_price = add_finishing_total + total_price;
                            }

                            //console.log("total_sq_cm..."+total_sq_cm);
                            var canvas_type = jQuery("input[name='canvas_type']:checked").val(); 
                            console.log("canvas_type..."+canvas_type);
                            if (canvas_type == "synthetic") {
                                jQuery(".lac_inner").hide();
                            }
                            if (canvas_type == "natural") {
                                jQuery(".lac_inner").show();
                            }

                            if (total_sq_cm == 600) {
                                
                                if (canvas_type == "natural") {
                                    total_price = parseFloat(total_price) + parseFloat(15.40);
                                    console.log("Inside Natural");
                                }else if(canvas_type == 'synthetic'){
                                    total_price = parseFloat(total_price) + parseFloat(8.90);
                                }

                            } else if (total_sq_cm == 1200) {
                                
                                if (canvas_type == "natural") {
                                    total_price = parseFloat(total_price) + parseFloat(19.00);
                                    console.log("Inside Natural");
                                }else if(canvas_type == 'synthetic'){
                                    total_price = parseFloat(total_price) + parseFloat(12.50);
                                }
                            } else if (total_sq_cm == 2000) {
                                if (canvas_type == "natural") {
                                    total_price = parseFloat(total_price) + parseFloat(20.00);
                                    console.log("Inside Natural");
                                }else if(canvas_type == 'synthetic'){
                                    total_price = parseFloat(total_price) + parseFloat(13.50);
                                }
                            }else if (total_sq_cm == 2400) {
                                if (canvas_type == "natural") {
                                    total_price = parseFloat(total_price) + parseFloat(22.70);
                                    console.log("Inside Natural");
                                }else if(canvas_type == 'synthetic'){
                                    total_price = parseFloat(total_price) + parseFloat(16.20);
                                }
                            }else if (total_sq_cm == 2800) {
                                if (canvas_type == "natural") {
                                    total_price = parseFloat(total_price) + parseFloat(23.90);
                                    console.log("Inside Natural");
                                }else if(canvas_type == 'synthetic'){
                                    total_price = parseFloat(total_price) + parseFloat(17.40);
                                }
                            } else if (total_sq_cm == 3000) {
                                if (canvas_type == "natural") {
                                    total_price = parseFloat(total_price) + parseFloat(24.90);
                                    console.log("Inside Natural");
                                }else if(canvas_type == 'synthetic'){
                                    total_price = parseFloat(total_price) + parseFloat(18.40);
                                }
                            }else if (total_sq_cm == 3500) {
                                if (canvas_type == "natural") {
                                    total_price = parseFloat(total_price) + parseFloat(29.50);
                                    console.log("Inside Natural");
                                }else if(canvas_type == 'synthetic'){
                                    total_price = parseFloat(total_price) + parseFloat(23.00);
                                }
                            }else if (total_sq_cm == 4500) {
                                if (canvas_type == "natural") {
                                    total_price = parseFloat(total_price) + parseFloat(30.90);
                                    console.log("Inside Natural");
                                }else if(canvas_type == 'synthetic'){
                                    total_price = parseFloat(total_price) + parseFloat(24.40);
                                }
                            }else if (total_sq_cm == 5400) {
                                if (canvas_type == "natural") {
                                    total_price = parseFloat(total_price) + parseFloat(35.50);
                                    console.log("Inside Natural");
                                }else if(canvas_type == 'synthetic'){
                                    total_price = parseFloat(total_price) + parseFloat(29.00);
                                }
                            }else if (total_sq_cm == 7000) {
                                if (canvas_type == "natural") {
                                    total_price = parseFloat(total_price) + parseFloat(41.50);
                                    console.log("Inside Natural");
                                }else if(canvas_type == 'synthetic'){
                                    total_price = parseFloat(total_price) + parseFloat(35.00);
                                }
                            }else if (total_sq_cm == 9600) {
                                if (canvas_type == "natural") {
                                    total_price = parseFloat(total_price) + parseFloat(47.50);
                                    console.log("Inside Natural");
                                }else if(canvas_type == 'synthetic'){
                                    total_price = parseFloat(total_price) + parseFloat(41.00);
                                }
                            }else if (total_sq_cm == 15000) {
                                if (canvas_type == "natural") {
                                    total_price = parseFloat(total_price) + parseFloat(66.50);
                                    console.log("Inside Natural");
                                }else if(canvas_type == 'synthetic'){
                                    total_price = parseFloat(total_price) + parseFloat(60.00);
                                }
                            }else if (total_sq_cm == 400) {
                                if (canvas_type == "natural") {
                                    total_price = parseFloat(total_price) + parseFloat(8.00);
                                    console.log("Inside Natural");
                                }else if(canvas_type == 'synthetic'){
                                    total_price = parseFloat(total_price) + parseFloat(14.50);
                                }
                            }else if (total_sq_cm == 900) {
                                if (canvas_type == "natural") {
                                    total_price = parseFloat(total_price) + parseFloat(18.00);
                                    console.log("Inside Natural");
                                }else if(canvas_type == 'synthetic'){
                                    total_price = parseFloat(total_price) + parseFloat(11.50);
                                }
                            }else if (total_sq_cm == 1600) {
                                if (canvas_type == "natural") {
                                    total_price = parseFloat(total_price) + parseFloat(21.50);
                                    console.log("Inside Natural");
                                }else if(canvas_type == 'synthetic'){
                                    total_price = parseFloat(total_price) + parseFloat(15.00);
                                }
                            }else if (total_sq_cm == 2500) {
                                if (canvas_type == "natural") {
                                    total_price = parseFloat(total_price) + parseFloat(24.00);
                                    console.log("Inside Natural");
                                }else if(canvas_type == 'synthetic'){
                                    total_price = parseFloat(total_price) + parseFloat(17.50);
                                }
                            }else if (total_sq_cm == 3600) {
                                if (canvas_type == "natural") {
                                    total_price = parseFloat(total_price) + parseFloat(30.20);
                                    console.log("Inside Natural");
                                }else if(canvas_type == 'synthetic'){
                                    total_price = parseFloat(total_price) + parseFloat(23.70);
                                }
                            }else if (total_sq_cm == 4900) {
                                if (canvas_type == "natural") {
                                    total_price = parseFloat(total_price) + parseFloat(30.70);
                                    console.log("Inside Natural");
                                }else if(canvas_type == 'synthetic'){
                                    total_price = parseFloat(total_price) + parseFloat(24.20);
                                }
                            }else if (total_sq_cm == 6400) {
                                if (canvas_type == "natural") {
                                    total_price = parseFloat(total_price) + parseFloat(37.50);
                                    console.log("Inside Natural");
                                }else if(canvas_type == 'synthetic'){
                                    total_price = parseFloat(total_price) + parseFloat(31.00);
                                }
                            }else if (total_sq_cm == 8100) {
                                if (canvas_type == "natural") {
                                    total_price = parseFloat(total_price) + parseFloat(40.50);
                                    console.log("Inside Natural");
                                }else if(canvas_type == 'synthetic'){
                                    total_price = parseFloat(total_price) + parseFloat(34.00);
                                }
                            }else if (total_sq_cm == 10000) {
                                if (canvas_type == "natural") {
                                    total_price = parseFloat(total_price) + parseFloat(51.50);
                                    console.log("Inside Natural");
                                }else if(canvas_type == 'synthetic'){
                                    total_price = parseFloat(total_price) + parseFloat(45.00);
                                }
                            }

                            
                            var lacquered_option = jQuery("input[name='material']:checked").val();
                            if (lacquered_option == 'lacquered') {
                                total_price = parseFloat(total_price) + parseFloat(5.00);
                                console.log(total_price);
                            } 

                            discount = 0.00;
                            console.log("total_price..."+total_price);
                            total_price = total_price.toFixed(2);

							              document.getElementById('total_product_price').value = total_price;
                            if (x > 0 && y > 0) {
                                
                                $('#target').Jcrop({boxWidth: '650', boxHeight: '900', setSelect: [0, 0, <?php echo $round_hundred_w; ?>, <?php echo $round_hundred_h; ?>], aspectRatio: x / y, onSelect: updateCoords});
                                jQuery('#lable_height').html("<p>"+y + "&nbsp;" + 'cm' +"</p>");
                                jQuery('#lable_width').html("<p>"+x + "&nbsp;" + 'cm' +"</p>");
                                jQuery('#total_price').text(total_price);
                                jQuery('#design_yout_wall_width').focusout();
                            }
                            
                            if (x == y){
                               console.log(x);
                               console.log(y);  
                               if(x == 20){
                                x1 = parseInt(x) - parseInt(50); 
                                y1 = parseInt(y) - parseInt(50); 
                               }if(x == 30){
                                x1 = parseInt(x) - parseInt(100); 
                                y1 = parseInt(y) - parseInt(100); 
                               }if(x == 40){
                                x1 = parseInt(x) - parseInt(150); 
                                y1 = parseInt(y) - parseInt(150); 
                               } 
                               
                                                              
                                $('#target').Jcrop({boxWidth: '600', boxHeight: '600', setSelect: [0, 0, <?php echo $round_hundred_w; ?>, <?php echo $round_hundred_h; ?>], aspectRatio: x1 / y1, onSelect: updateCoords,trueSize: [600,600]});
                                
                                //$(".jcrop-selection.jcrop-current").width(x1).height(y1);
                                //$(".jcrop-selection.jcrop-current").addClass("jcrop-focus");

                                jQuery('#lable_height').html("<p>"+y + "&nbsp;" + 'cm' +"</p>");
                                jQuery('#lable_width').html("<p>"+x + "&nbsp;" + 'cm' +"</p>");
                                jQuery('#total_price').text(total_price);
                                jQuery('#design_yout_wall_width').focusout();
                            }
                           
                        });

                    });
                </script>

				 <div id="lable_height" style="color: #FFF; font-weight: bold; height: 200px; width: 29px; z-index: 99; position: absolute; left: 0px; bottom: 0px; background: url('http://printedwalls.co.uk/wp-content/uploads/2016/09/height-size.jpg') no-repeat;"><p><?php echo get_option('wdyw_default_height'); ?>&nbsp;Cm</p></div>  
                <div id="interface" class="page-interface" style="position:relative;"><img src="<?php echo $attachment->guid; ?>" id="target">
				</div>
				<div id="lable_width" style="color: #FFF; font-weight: bold; height: 29px; width: 200px; position: absolute; right: 267px; top: 6px; z-index: 99; background: url('http://printedwalls.co.uk/wp-content/uploads/2016/09/width-size.jpg') no-repeat;"><p><?php echo get_option('wdyw_default_width'); ?>&nbsp;Cm</p></div>
               
                <div class="nav-box">
                    <form onsubmit="return false;" id="text-inputs"><span class="input-group"><b>X</b>
                            <input type="text" name="cx" id="crop-x" class="span1"></span><span class="input-group"><b>Y</b>
                            <input type="text" name="cy" id="crop-y" class="span1"></span><span class="input-group"><b>W</b>
                            <input type="text" name="cw" id="crop-w" class="span1"></span><span class="input-group"><b>H</b>
                            <input type="text" name="ch" id="crop-h" class="span1"></span><span class="input-group"><b>Image Path</b>
                            <input type="text" value="<?php echo $attachment_path; ?>" name="ch" id="image-path" class="span1"></span>
                    </form>
                </div>                                                
                <div class="row small-up-4">
                </div>

        </div>

        </div>
        <div class="wall_size col small-12 medium-12 large-3">
            <div class="wall_unit">
			
                <div class="row" style="display: none;">
                    <div class="small-3 columns">
                        <label for="middle-label" class="middle">Canvas Size: </label>
                    </div>
                    <div class="small-9 columns">
                        <select id="unit" onchange="javascript:unitConversion(this.value);">                            
                            <option selected="selected" value="foot">Feet</option>
                            <option value="cm">CM</option>
                            <option value="m">Meter</option>
                            <option value="inches">Inches</option>
                            <option value="mm">MM</option>
                        </select>
                    </div>
                </div>

                <div class="row">
                    
                    
                    <div class="col medium-12 large-12 options_right">
                        <div class="row">
                            
                            <div class="col small-12 edgeof_canvas_main">
                                <div class="">
                                    <h3>Edges of Canvas</h3>
                                </div>
                                <div class="edges_radios">
                                    <!--<input id="resume_photo" type="radio" onclick="getSelectedBorder();" name="edges_of_canvas" value="resume_photo"><label for="resume_photo">Resume photo</label><br>
                                    <input id="mirror_image"id="30x20"type="radio" onclick="getSelectedBorder();" name="edges_of_canvas" value="mirror_image"><label for="mirror_image">Mirror image</label><br>
                                    <input id="solid_color"type="radio" onclick="getSelectedBorder();" name="edges_of_canvas" value="solid_color"><label for="solid_color">Solid Color</label> <input type='text' id="select_border"/><br> -->
                                    <div class="edges_radios_inner"><input id="mirror_wrap"type="radio" onclick="getSelectedBorder();" name="edges_of_canvas" value="mirror wrap" checked><label for="mirror_wrap"><div class="wrapleft"><img class="edges_mirror_wrap" src="<?php echo plugins_url('wc-design-your-wall/assets/images/mirror-wrap.png'); ?>"></div> <div class="wrapright"><h3>Mirror wrap</h3><p>We will select the edges of your photo and flip them onto the sides to give you that image wrap look without compromising any important elements.</p></div></label></div>

                                    <div class="edges_radios_inner"><input id="color_wrap"type="radio" onclick="getSelectedBorder();" name="edges_of_canvas" value="color wrap"><label for="color_wrap"><div class="wrapleft"><img class="edges_mirror_wrap" src="<?php echo plugins_url('wc-design-your-wall/assets/images/color-wrap.png'); ?>"><input style="margin-top: 20px;" type='text' id="select_border"/></div> <div class="wrapright"><h3>Color wrap</h3><p>We will select the edges of your photo and flip them onto the sides to give you that image wrap look without compromising any important elements.</p></div></label></div>

                                    <div class="edges_radios_inner"><input id="image_wrap"type="radio" onclick="getSelectedBorder();" name="edges_of_canvas" value="image wrap"><label for="image_wrap"><div class="wrapleft"><img class="edges_mirror_wrap" src="<?php echo plugins_url('wc-design-your-wall/assets/images/image-wrap.png'); ?>"></div> <div class="wrapright"><h3>Image wrap</h3><p>We will select the edges of your photo and flip them onto the sides to give you that image wrap look without compromising any important elements.</p></div></label></div>
                                </div>
								<input id="selected_edges_of_canvas" type="text" name="selected_edges_of_canvas" value="" style="display:none;">
								<input id="selected_color_wrap" type="text" name="selected_color_wrap" value="" style="display:none;">
                            </div>
                        </div>
                        <div class="row add_cart_main">
                            <div class="col small-12">
                            <input type="text" id="total_product_price" value="0" style="display:none;" />
                            <input type="text" id="delivery_total_price" value="0" style="display:none;" />
                            <input type="text" id="material_total_price" value="0" style="display:none;" />
                            <input type="text" id="finishing_total_price" value="0" style="display:none;" />
                            <input type="text" id="resetJcrop" onclick="javascript:updateFinishingPrice()" style="display:none;" />
                        
                            <div class="wall_price">Price : <span ><?php echo get_woocommerce_currency_symbol(); ?><span id="total_price"><?php echo get_option('wdyw_min_price') * get_option('wdyw_default_width') * get_option('wdyw_default_height'); ?></span></span></div>
                                <div class="add_cart_container">
                                   <!-- <a id="wdyw_add_to_cart" onclick="wdyw_add_to_cart()"><img src="<?php //echo plugins_url() . '/wc-design-your-wall/assets/images/add-to-cart-img.png'; ?>" /></a>-->
                                    <button type="button" id="wdyw_add_to_cart" onclick="wdyw_add_to_cart()" class="button primary is-bevel"><i class="fas fa-shopping-cart"></i> Add to cart</button>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="width_main" style="display: none;">
                        <div class="">
                            <label for="design_yout_wall_width" class="middle">Width: </label>
                        </div>
                        <div class="">
                            <input type="text" id="design_yout_wall_width" value="<?php echo get_option('wdyw_default_width'); ?>">
                        </div>
                    </div>
                    <div class="height_main" style="display: none;">
                        <div class="">
                            <label for="design_yout_wall_height" class="middle">Height: </label>
                        </div>
                        <div class="">
                            <input type="text" id="design_yout_wall_height" value="<?php echo get_option('wdyw_default_height'); ?>">
                        </div>
                    </div>
    				
                </div>
            </div>
				<div class="row" id="deliver_option" style="display: none;">
                    <div class="col small-4">
                        <label for="middle-label" class="middle">Delivery Options: </label>
                    </div>
                    <div class="col small-8">
                        <select name="delivery" id="delivery" onchange="set_delivery()">
                            <option value="standard" selected>Standard</option>
							<option value="72hrs">72hrs</option>
                            <option value="48hrs">48hrs</option>                            
                                                        
                        </select>
                    </div>
                </div>
				
                
        </div>
    </div>
    </div>

<?php endwhile; ?>

<?php get_template_part('footer-top'); ?>
<?php get_footer(); ?>