var curunit = "cm";

// function unitConversion(e) {
//     console.log("unit conversion selected...");
//     var t = document.getElementById("design_yout_wall_width").value,
//         i = document.getElementById("design_yout_wall_height").value;
    
//     if ("cm" == e) {
//         if ("mm" == curunit)
//             var n = t / 10,
//                 u = i / 10;
//         else if ("m" == curunit)
//             var n = 100 * t,
//                 u = 100 * i;
//         else if ("foot" == curunit)
//             var n = 30.48 * t,
//                 u = 30.48 * i;
//         else if ("inches" == curunit)
//             var n = t / .3937,
//                 u = i / .3937;
//         curunit = "cm", unit_name = "CM"
//     } else if ("m" == e) {
//         if ("mm" == curunit)
//             var n = t / 1e3,
//                 u = i / 1e3;
//         else if ("cm" == curunit)
//             var n = t / 100,
//                 u = i / 100;
//         else if ("foot" == curunit)
//             var n = .3048 * t,
//                 u = .3048 * i;
//         else if ("inches" == curunit)
//             var n = t / 39.370078740157,
//                 u = i / 39.370078740157;
//         curunit = "m", unit_name = "M"
//     } else if ("mm" == e) {
//         if ("cm" == curunit)
//             var n = 10 * t,
//                 u = 10 * i;
//         else if ("m" == curunit)
//             var n = 1e3 * t,
//                 u = 1e3 * i;
//         else if ("foot" == curunit)
//             var n = 304.8 * t,
//                 u = 304.8 * i;
//         else if ("inches" == curunit)
//             var n = t / .03937,
//                 u = i / .03937;
//         curunit = "mm", unit_name = "MM"
//     } else if ("foot" == e) {
//         if ("cm" == curunit)
//             var n = t / 30.48,
//                 u = i / 30.48;
//         else if ("m" == curunit)
//             var n = t / .3048,
//                 u = i / .3048;
//         else if ("mm" == curunit)
//             var n = t / 304.8,
//                 u = i / 304.8;
//         else if ("inches" == curunit)
//             var n = t / 12,
//                 u = i / 12;
//         curunit = "foot", unit_name = "Ft"
//     } else if ("inches" == e) {
//         if ("foot" == curunit)
//             var n = 12 * t,
//                 u = 12 * i;
//         else if ("cm" == curunit)
//             var n = .3937 * t,
//                 u = .3937 * i;
//         else if ("m" == curunit)
//             var n = 39.370078740157 * t,
//                 u = 39.370078740157 * i;
//         else if ("mm" == curunit)
//             var n = .03937 * t,
//                 u = .03937 * i;
//         curunit = "inches", unit_name = "In"
//     }
//     n = n.toFixed(2), u = u.toFixed(2), document.getElementById("design_yout_wall_width").value = n, document.getElementById("design_yout_wall_height").value = u, jQuery(".unit_value").text(unit_name), jQuery("#lable_height").html("<p>"+ n + "&nbsp;" + unit_name+"</p>"), jQuery("#lable_width").html("<p>"+ u + "&nbsp;" + unit_name+"</p>")
// }

jQuery(document).ready(function(){
    //jQuery("type:radio[name=canvas_type]").attr("checked", true).trigger("click");
    jQuery( 'input[name="canvas_type"]:radio:first' ).click();

    jQuery(".horizontal_main").hide();
    jQuery(".square_main").hide();

    if( jQuery('#total_price').length )        
    {
        var total_price = document.getElementById('total_price').innerHTML;
        var product_price_total = document.getElementById('total_product_price').value = total_price;
    }
    
});
    
    //Get selected value of Canvas size

    function getSelectedSize(selected_sizecanvas) {
        var selected_size = selected_sizecanvas.value.split('x');
        var selected_width = selected_size[0];
        var selected_height = selected_size[1];

        jQuery("#design_yout_wall_width").val(selected_width);
        jQuery("#design_yout_wall_height").val(selected_height);

        console.log(selected_size[0]);
        console.log(selected_size[1]);

        jQuery("#design_yout_wall_width").keyup();
        jQuery("#design_yout_wall_height").keyup();       
    }

    function canvasSize(selected_size){
        //console.log(selected_size.value);
        jQuery(".vertical_main").hide();
        jQuery(".horizontal_main").hide();
        jQuery(".square_main").hide();

        if (selected_size.value == 'vertical') {
            jQuery(".vertical_main").show();
            jQuery("#20x30").prop("checked", true); 
            jQuery('#20x30').click();
        }if (selected_size.value == 'horizontal') {
            jQuery(".horizontal_main").show();
            jQuery("#30x20").prop("checked", true);
            jQuery('#30x20').click(); 
        }if (selected_size.value == 'square') {
            jQuery(".square_main").show();
            jQuery("#20x20").prop("checked", true);
            jQuery('#20x20').click();
        }

    }    
function set_finishing(){
	var product_price = document.getElementById('total_product_price').value;
	var finishing = document.getElementById('finishing').value;
	//console.log("product_price..."+product_price);

	if(finishing=='one_piece'){
		finishing_price = parseInt(20);  //adding 20$ to product price
        console.log("finishing_price..." + finishing_price);
	}else if(finishing=='tiles'){
		finishing_price = '0';
	}
	var finishing_total_price = document.getElementById('finishing_total_price').value = finishing_price;
	calculate_total();
}
function set_material(){
	var product_price = document.getElementById('total_product_price').value;
	var material = document.getElementById('material').value;
	//console.log(material);
	if(material=='polycril_paper'){
		//material_price = product_price * 10 / 100;
        material_price = 5;
		total_price = parseFloat(material_price) + parseFloat(product_price);
	}else if(material=='pvc'){
		material_price = '0';
		total_price =parseFloat(product_price);
	}
	//var material_total_price = document.getElementById('material_total_price').value = material_price;
	calculate_total();
}

// jQuery("#select_border").click(function() {
//     var selected_solid_color = jQuery("#select_border").spectrum('get');
//     console.log(selected_solid_color);
// });

function getSelectedBorder(){

    var sel_edge_canvas = jQuery('input[name="edges_of_canvas"]:checked').val();
   jQuery('#selected_edges_of_canvas').val(sel_edge_canvas);
    if (sel_edge_canvas == 'color wrap') {
        var selected_solid_color_str = jQuery("#select_border").spectrum("get").toHexString();
		var selected_solid_color = selected_solid_color_str.replace('#','');
			console.log(selected_solid_color);		
         jQuery('#selected_color_wrap').val(selected_solid_color);
    }else{
		selected_solid_color = '';
		jQuery('#selected_color_wrap').val(selected_solid_color);
	}
}
function set_delivery(){
	var product_price = document.getElementById('total_product_price').value;
	var delivery = document.getElementById('delivery').value;
	if(delivery == '72hrs'){
		delivery_price = product_price * 15 / 100;
		total_price = parseFloat(delivery_price) + parseFloat(product_price);
	}else if(delivery == '48hrs'){
		delivery_price = product_price * 25 / 100;
		total_price = parseFloat(delivery_price) + parseFloat(product_price);
	}else if(delivery=='standard'){
		delivery_price = '0';
		total_price = parseFloat(product_price);
	}
	var delivery_total_price = document.getElementById('delivery_total_price').value = delivery_price;
	calculate_total();
}
function calculate_total(){
	var product_price = document.getElementById('total_product_price').value;
	finishing_total_price = document.getElementById('finishing_total_price').value;
	delivery_total_price = document.getElementById('delivery_total_price').value;
	material_total_price = document.getElementById('material_total_price').value;
	//console.log(product_price);
	//console.log(delivery_total_price);
	//console.log(material_total_price);
	//console.log(finishing_total_price);
	var total_price = parseFloat(product_price) + parseFloat(delivery_total_price)+ parseFloat(material_total_price) + parseFloat(finishing_total_price);
	
	document.getElementById('total_price').innerHTML = total_price.toFixed(2);	
}
function updateCoords(e) {
}

function wdyw_add_to_cart() {
    var e = .3 * min_width,
            t = .3 * min_height,
            i = 304.8 * min_width,
            n = 304.8 * min_height,
            u = 12 * min_width,
            r = 12 * min_height,
            a = 30.48 * min_width,
            l = 30.48 * min_height,
            m = document.getElementById("design_yout_wall_width").value,
            c = document.getElementById("design_yout_wall_height").value,
            o = jQuery("#unit").val();

    var me = .3 * max_width,
            mt = .3 * max_height,
            mi = 304.8 * max_width,
            mn = 304.8 * max_height,
            mu = 12 * max_width,
            mr = 12 * max_height,
            ma = 30.48 * max_width,
            ml = 30.48 * max_height;

    /*switch (o) {
        case "foot":
            if (parseInt(m) < parseInt(min_width) || parseInt(c) < parseInt(min_height))
                return alert("Width and Height must be greater then or equal to " + min_width + " x " + min_height + " Foot"), !1;
            if (parseInt(m) > parseInt(max_width) || parseInt(c) > parseInt(max_height))
                return alert("Width and Height must be less then or equal to " + max_width + " x " + max_height + " Foot"), !1;
            break;
        case "cm":
            if (a > m || l > c)
                return alert("Width and Height must be greater then or equal to " + a + " x " + l + " CM"), !1;
            if (ma < m || ml < c)
                return alert("Width and Height must be less then or equal to " + ma + " x " + ml + " CM"), !1;
            break;
        case "m":
            if (e > m || t > c)
                return alert("Width and Height must be greater then or equal to " + e + " x " + t + " Meters"), !1;
            if (me < m || mt < c)
                return alert("Width and Height must be less then or equal to " + me + " x " + mt + " Meters"), !1;
            break;
        case "mm":
            if (i > m || n > c)
                return alert("Width and Height must be greater then or equal to " + i + " x " + n + " Mili Meters"), !1;
            if (mi < m || mn < c)
                return alert("Width and Height must be less then or equal to " + mi + " x " + mn + " Mili Meters"), !1;
            break;
        case "inches":
            if (u > m || r > c)
                return alert("Width and Height must be greater then or equal to " + u + " x " + r + " Inches"), !1
            if (mu < m || mr < c)
                return alert("Width and Height must be less then or equal to " + mu + " x " + mr + " Inches"), !1
    }*/
    var d = document.getElementById("crop-x").value,
            h = document.getElementById("crop-y").value,
            s = document.getElementById("crop-w").value,
            _ = document.getElementById("crop-h").value,
            f = document.getElementById("image-path").value,
            g = 'cm',
            //v = document.getElementById("canvas_type").value,
            v = jQuery("input[name='canvas_type']:checked").val(),
            edges_of_canvas = document.getElementById("selected_edges_of_canvas").value,
            color_wrap = document.getElementById("selected_color_wrap").value,
            material = jQuery("input[name='material']:checked").val(),
            delvr = document.getElementById("delivery").value,
            wt = jQuery("#wallpaper_title").text(),
            y = jQuery("#total_price").text(),
            w = jQuery("#target").attr("src"),
            url = window.location.href,
            I = {
                action: "wdyw_cart_handler",
                data: {
                    x: d,
                    y: h,
                    w: s,
                    h: _,
                    image: f
                }
            };
    jQuery.post(ajaxurl, I, function(e) {
        var t = JSON.parse(e);
        var wallpaper_url = window.location.href;
        window.location = "?add-to-cart=" + t.product_id + "&filename=" + t.filename + "&width=" + m + "&height=" + c + "&unit=" + g + "&finishing=" + v + "&delivery_opt=" + delvr + "&material=" + material+ "&color_wrap=" + color_wrap + "&edges_of_canvas=" + edges_of_canvas + "&total_price=" + y + "&full_image=" + w + "&wallpaper_title=" + wt + "&wallpaper_url=" + url + "&wallpaper_url=" + wallpaper_url
    })
}

function updateFinishingPrice() {
    jQuery("#design_yout_wall_width").keyup()
}

jQuery(document).ready(function() {
    jQuery('.cart .cart_item .product-thumbnail').each(function() {
        var product_url = jQuery(this).next('.product-name').children('a').attr('href');
        jQuery('.cart .product-thumbnail a').attr('href', product_url);
    });

    var count = 0;
    setInterval(function() {

        if (jQuery('.woocommerce-checkout-review-order-table tbody .cart_item .product-name').hasClass('added')) {
            count = 1;
        }

        if (count == 0) {
            jQuery('.woocommerce-checkout-review-order-table tbody .cart_item .product-name').html(jQuery('.checkout_wallpaper').html());
            jQuery('.woocommerce-checkout-review-order-table tbody .cart_item .product-name').addClass('added');
            jQuery('.woocommerce-checkout-review-order-table tbody .cart_item').hide();
            jQuery('.woocommerce-checkout-review-order-table tbody .cart_item:first-child').show();           
            jQuery('.woocommerce-checkout-review-order-table tbody .cart_item .product-total').text(jQuery('.cart-subtotal .amount').text());
        }
    }, 3000);

});
jQuery(document).ready(function() {
    jQuery("#select_border").spectrum({
        color: "#ECC",
        showInput: true,
        className: "full-spectrum",
        showInitial: true,
        showPalette: true,
        showSelectionPalette: true,
        maxSelectionSize: 10,
        preferredFormat: "hex",
        localStorageKey: "spectrum.demo",
        move: function (color) {
            
        },
        show: function () {
        
        },
        beforeShow: function () {
        
        },
        hide: function () {
        
        },
        change: function() {
            getSelectedBorder();
        },
        palette: [
            ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
            "rgb(204, 204, 204)", "rgb(217, 217, 217)","rgb(255, 255, 255)"],
            ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
            "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"], 
            ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)", 
            "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)", 
            "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)", 
            "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)", 
            "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)", 
            "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
            "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
            "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
            "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)", 
            "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
        ]
    });
});