<?php
if ($query->have_posts()) :
    ?>

    <h1 class="page-title">Select Your Design</h1>

    <table border="0" cellpadding="5" cellspacing="5" width="100%">
        <tr>
            <?php
            $x = 1;
            while ($query->have_posts()) : $query->the_post();
                ?>
                <td width="33%">
                    <?php the_post_thumbnail('thumbnail'); ?><br />
                    <?php the_title(sprintf('<a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a>'); ?>
                </td>  
                <?php
                if ($x == 4) {
                    echo "</tr><tr>";
                }
                $x++;
            endwhile;
            ?>
        </tr>
    </table>
    <?php
 endif;
 