<div class="wrap">
    <h1>Wallpapers Settings</h1>
    <?php
    if ($updated) {
        echo '<div class="updated">';
        echo '<p>Settings Saved!</p>';
        echo '</div>';
    }
    ?>
    <form method="post">
        <table class="form-table">
            <tbody>
                <tr>
                    <th scope="row"><label for="wdyw_product">Select Product for Wall Design:</label></th>
                    <td>
                        <select name="wdyw_product" id="wdyw_product">
                            <option value="">Select Product</option>
                            <?php
                            foreach ($products as $product) {
                                $selected = NULL;
                                if ($product[2] == $_POST['wdyw_product'] || $product[2] == $current_product) {
                                    $selected = 'selected="selected"';
                                }
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $product[2]; ?>"><?php echo $product[0]; ?></option>
                                <?php
                            }
                            ?>
                        </select>                        
                    </td>
                </tr>

                <tr>
                    <th scope="row"><label for="wdyw_custom_wallpaper">Select Custom Wallpaper:</label></th>
                    <td>
                        <select name="wdyw_custom_wallpaper" id="wdyw_custom_wallpaper">
                            <option value="">Select Custom Wallpaper</option>
                            <?php
                            if ($wallpapers->have_posts()) :
                                while ($wallpapers->have_posts()) : $wallpapers->the_post();
                                    $selected = NULL;
                                    if (get_the_ID() == $_POST['wdyw_custom_wallpaper'] || get_the_ID() == $current_wallpaper) {
                                        $selected = 'selected="selected"';
                                    }
                                    ?>
                                    <option <?php echo $selected; ?> value="<?php echo get_the_ID(); ?>"><?php the_title(); ?></option>
                                    <?php
                                endwhile;
                            endif;
                            ?>
                        </select>                        
                    </td>
                </tr>

                <tr>
                    <th scope="row"><label for="wdyw_default_width">Default Width (Foot):</label></th>
                    <td>
                        <input type="text" name="wdyw_default_width" id="wdyw_default_width" value="<?php echo get_option('wdyw_default_width'); ?>">                        
                    </td>
                </tr>

                <tr>
                    <th scope="row"><label for="wdyw_default_height">Default Height (Foot):</label></th>
                    <td>
                        <input type="text" name="wdyw_default_height" id="wdyw_default_height" value="<?php echo get_option('wdyw_default_height'); ?>">                        
                    </td>
                </tr>
                
                <tr>
                    <th scope="row"><label for="wdyw_minimum_width">Min Width (Foot):</label></th>
                    <td>
                        <input type="text" name="wdyw_minimum_width" id="wdyw_minimum_width" value="<?php echo get_option('wdyw_minimum_width'); ?>">                        
                    </td>
                </tr>

                <tr>
                    <th scope="row"><label for="wdyw_minimum_height">Min Height (Foot):</label></th>
                    <td>
                        <input type="text" name="wdyw_minimum_height" id="wdyw_minimum_height" value="<?php echo get_option('wdyw_minimum_height'); ?>">                        
                    </td>
                </tr>
                
                <tr>
                    <th scope="row"><label for="wdyw_max_width">Max Width (Foot):</label></th>
                    <td>
                        <input type="text" name="wdyw_max_width" id="wdyw_max_width" value="<?php echo get_option('wdyw_max_width'); ?>">                        
                    </td>
                </tr>

                <tr>
                    <th scope="row"><label for="wdyw_max_height">Max Height (Foot):</label></th>
                    <td>
                        <input type="text" name="wdyw_max_height" id="wdyw_max_height" value="<?php echo get_option('wdyw_max_height'); ?>">                        
                    </td>
                </tr>

                <tr>
                    <th scope="row"><label for="wdyw_default_height">Per Squre Feet Price:</label></th>
                    <td>
                        <input type="text" name="wdyw_min_price" id="wdyw_min_price" value="<?php echo get_option('wdyw_min_price'); ?>">                        
                    </td>
                </tr>

                <tr>
                    <th scope="row"><label for="wdyw_default_height">Increase price on one piece (%):</label></th>
                    <td>
                        <input type="text" name="wdyw_finishing_one_piece" id="wdyw_finishing_one_piece" value="<?php echo get_option('wdyw_finishing_one_piece'); ?>">                        
                    </td>                    
                </tr>                                

                <tr>                    
                    <td>
                        <h3>Quantity Discounts</h3>                        
                    </td>                    
                </tr>

                <tr>
                    <th scope="row"><label for="wdyw_100_200_discount">100 - 200 Sq. Feet (%):</label></th>
                    <td>
                        <input type="text" name="wdyw_100_200_discount" id="wdyw_100_200_discount" value="<?php echo get_option('wdyw_100_200_discount'); ?>">                        
                    </td>                    
                </tr>

                <tr>
                    <th scope="row"><label for="wdyw_201_300_discount">201 - 300 Sq. Feet (%):</label></th>
                    <td>
                        <input type="text" name="wdyw_201_300_discount" id="wdyw_201_300_discount" value="<?php echo get_option('wdyw_201_300_discount'); ?>">                        
                    </td>                    
                </tr>

                <tr>
                    <th scope="row"><label for="wdyw_301_400_discount">301 - 400 Sq. Feet (%):</label></th>
                    <td>
                        <input type="text" name="wdyw_301_400_discount" id="wdyw_301_400_discount" value="<?php echo get_option('wdyw_301_400_discount'); ?>">                        
                    </td>                    
                </tr>

                <tr>
                    <th scope="row"><label for="wdyw_401_500_discount">401 - 500 Sq. Feet (%):</label></th>
                    <td>
                        <input type="text" name="wdyw_401_500_discount" id="wdyw_401_500_discount" value="<?php echo get_option('wdyw_401_500_discount'); ?>">                        
                    </td>                    
                </tr>

                <tr>
                    <th scope="row"><label for="wdyw_500_plus_discount">500+ Sq. Feet (%):</label></th>
                    <td>
                        <input type="text" name="wdyw_500_plus_discount" id="wdyw_500_plus_discount" value="<?php echo get_option('wdyw_500_plus_discount'); ?>">                        
                    </td>                    
                </tr>


            </tbody>
        </table>
        <p class="submit"><input type="submit" name="wdyw_save_settings" id="submit" class="button button-primary" value="Save Settings"></p>
    </form>    
</div>