<?php
/*
  Plugin Name: WooCommerce Design Your Wall
  Plugin URI: http://solzsoft.com/
  Description: WooCommerce Design Your Wall
  Version: 1.0
  Author Name: Mustafa Jamal
  Author URI: http://solzsoft.com/
 */
if (!defined('DS')) {
    define("DS", DIRECTORY_SEPARATOR);
}
if (!defined('WP_MEMORY_LIMIT')) {
    define('WP_MEMORY_LIMIT', "1024M");
}
if (!defined('VERSION')) {
    define("VERSION", '1.0');
}


include(dirname(__FILE__) . DS . 'includes' . DS . 'class.wdyw.php');
add_action('init', array('WDYW', 'init'));